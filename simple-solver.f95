PROGRAM SimpleSolver
USE simple_solver_lib
IMPLICIT NONE

REAL, DIMENSION(2, 2) :: EqMatrix
REAL, DIMENSION(1, 2) :: AnsMatrix
REAL :: x, y

CALL GetEquations(EqMatrix, AnsMatrix)

! The hard way (manually)
CALL CalcHardWay(EqMatrix, AnsMatrix, x, y)
CALL PrintResults("Hard Way", x, y)

! The easy way (built in)
CALL CalcEasyWay(EqMatrix, AnsMatrix, x, y)
CALL PrintResults("Easy Way", x, y)

END
