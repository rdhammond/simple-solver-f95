MODULE simple_solver_lib
CONTAINS
  SUBROUTINE GetEquations(eq, ans)
    REAL, DIMENSION(2,2), INTENT(OUT) :: eq
    REAL, DIMENSION(1,2), INTENT(OUT) :: ans

    PRINT *, "Equation 1: x y ="
    READ *, eq(1,1), eq(1,2), ans(1,1)
    PRINT *, "Equation 2: x y ="
    READ *, eq(2,1), eq(2,2), ans(1,2)
  END

  SUBROUTINE SwapValues(A, x1, y1, x2, y2, flipSigns)
    REAL, DIMENSION(2,2), INTENT(INOUT) :: A
    INTEGER, INTENT(IN) :: x1, y1, x2, y2
    LOGICAL, OPTIONAL, INTENT(IN) :: flipSigns

    REAL :: temp
    LOGICAL :: fs

    IF (PRESENT(flipSigns)) THEN
      fs = flipSigns
    ELSE
      fs = .FALSE.
    END IF

    temp = A(x1,y1)
    A(x1,y1) = A(x2,y2)
    A(x2,y2) = temp

    IF (fs) THEN
      A(x1,y1) = -A(x1,y1)
      A(x2,y2) = -A(x2,y2)
    END IF
  END

  SUBROUTINE InvertMatrix(A)
    REAL, DIMENSION(2,2), INTENT(INOUT) :: A

    REAL :: invFactor

    invFactor = 1 / (A(1,1) * A(2,2) - A(1,2) * A(2,1))
    CALL SwapValues(A, 1, 1, 2, 2)
    CALL SwapValues(A, 1, 2, 2, 1, flipSigns=.TRUE.)
    A = A * invFactor
  END

  SUBROUTINE CalcHardWay(eq, ans, x, y)
    REAL, DIMENSION(2,2), INTENT(IN) :: eq
    REAL, DIMENSION(1,2), INTENT(IN) :: ans
    REAL, INTENT(OUT) :: x, y

	REAL, DIMENSION(2,2) :: eqCopy
	
	eqCopy = eq
    CALL InvertMatrix(eqCopy)
    x = (eqCopy(1,1) * ans(1,1) + eqCopy(2,1) * ans(1,2))
    y = (eqCopy(1,2) * ans(1,1) + eqCopy(2,2) * ans(1,2))
  END

  SUBROUTINE PrintResults(calcType, x, y)
	CHARACTER (LEN=*) :: calcType
	REAL, INTENT(IN) :: x, y

	print *, "Type: ", calcType
	print *
    print *, "x=", x, " y=", y
    print *
  END

  SUBROUTINE CalcEasyWay(eq, ans, x, y)
    REAL, DIMENSION(2,2), INTENT(IN) :: eq
    REAL, DIMENSION(1,2), INTENT(IN) :: ans
    REAL, INTENT(OUT) :: x, y

	REAL, DIMENSION(2,2) :: eqCopy
	REAL, DIMENSION(1,2) :: res
	
	eqCopy = eq
    CALL InvertMatrix(eqCopy)
	res = matmul(ans, eqCopy)
	x = res(1,1)
	y = res(1,2)
  END
END