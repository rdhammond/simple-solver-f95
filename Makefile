simple-solver: simple-solver-lib.o simple-solver.o
	gfortran -o simple-solver simple-solver-lib.o simple-solver.o

simple-solver-lib.o: simple-solver-lib.f95
	gfortran -c simple-solver-lib.f95

simple_solver_lib.mod: simple-solver-lib.o simple-solver-lib.f95
	gfortran -c simple-solver-lib.f95

simple-solver.o: simple-solver.f95
	gfortran -c simple-solver.f95

clean:
	rm -f *.mod *.o simple-solver
